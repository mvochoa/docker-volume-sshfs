module gitlab.com/mvochoa/docker-volume-sshfs

go 1.15

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-plugins-helpers v0.0.0-20200102110956-c9a8a2d92ccc
	github.com/sirupsen/logrus v1.8.0
	golang.org/x/net v0.0.0-20210220033124-5f55cee0dc0d // indirect
)
