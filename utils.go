package main

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

func logError(format string, args ...interface{}) error {
	logrus.Errorf(format, args...)
	return fmt.Errorf(format, args...)
}
